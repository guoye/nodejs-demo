
const express = require('express');
const router = express.Router();
const fs = require('fs'); // 文件系统库
const multer = require('multer'); // 文件上传中间件库 需要npm进行安装

/**
 * 使用中文文档 https://github.com/expressjs/multer/blob/master/doc/README-zh-cn.md
 * 配置上传路径
 */
const upload = multer({
    dest: 'uploads/'
});
// 配置上传中间件
const uploadMiddleware = upload.fields([{ name: 'files', maxCount: 2 }]);

/**
 * 第一方案：临时变量存档上传数据，注意express重启后会清0
 * 第二方案：可以利用fs文件系统将数据写入json来替换第一方案，express重启后数据还在
 * 下面是第一方案
 */
const filesArr = [];
let title = '上传图片(GET请求)';

// 渲染页面
router.get('/upload', function (req, res) {
    res.render('upload', { filesArr: filesArr, title: title});
});

// 上传图片业务逻辑
/* router.post('/upload', uploadMiddleware, function (req, res) {
    const list = req.files.files;
    for (let i = 0; i < req.files.files.length; i++) {
        filesArr.push(list[i])
    }
    // 上传成功后，重定向 /upload 进行页面渲染
    res.redirect('back');
}); */

// 上传图片业务逻辑，错误处理机制
router.post('/upload', function (req, res) {
    uploadMiddleware(req, res, function (err) {
        if (err) {
            title = `上传失败，错误代码：${err.code}(GET请求)`;
            res.redirect('back');
            return
        }

        const list = req.files.files;
        if (list){
            for (let i = 0; i < req.files.files.length; i++) {
                filesArr.push(list[i])
            }
            title = '上传成功!';
        }
        // 上传成功后，重定向 /upload 进行页面渲染
        res.redirect('back');
    })
});

// 在upload.ejs中 “<img src=” 获取图片 的请求
router.get('/img/:filename', function(req, res){
    /**
     * req.params.filename 上传图片时的filename
     * createReadStream方法根据路径创建[流图片]  使用说明：http://nodejs.cn/api/fs.html#fs_fs_createreadstream_path_options
     * 使用pipe插入response中
     */
    const img = fs.createReadStream('uploads/' + req.params.filename);
    img.pipe(res);
});


module.exports = router;